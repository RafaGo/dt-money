import Modal from 'react-modal';
import { Container, TransactionTypeContainer, RadioBox } from './styles';
import fechar from '../../assets/fechar.svg'
import saida from '../../assets/saidas.svg'
import entrada from '../../assets/entradas.svg'
import { FormEvent, useContext, useState } from 'react';
import { api } from '../../service/api';
import { TransactionsContext } from '../../TransactionsContext';



interface TransactionModalProps {
    isOpen: boolean;
    onRequestClose: () => void;
}

export function TransactionModal({ isOpen, onRequestClose }: TransactionModalProps) {

    const { createTransaction } = useContext(TransactionsContext);

    const [type, setType] = useState('deposit')
    const [title, setTitle] = useState('')
    const [amount, setAmount] = useState(0)
    const [category, setCategory] = useState('')

    async function handleActionForm(event: FormEvent) {
        event.preventDefault()

        await createTransaction({
            title,
            amount,
            category,
            type
        })
        
        setTitle('')
        setType('')
        setAmount(0)
        setCategory('')
        onRequestClose();

    };

    return (
        <Modal
            isOpen={isOpen}
            onRequestClose={onRequestClose}
            overlayClassName="react-modal-overlay"
            className="react-modal-content"
        >
            <Container onSubmit={handleActionForm}>
                <h2>Cadastrar Transação</h2>
                <input
                    placeholder='Titulo'
                    value={title}
                    onChange={event => setTitle(event.target.value)}
                />
                <button
                    type='button'
                    onClick={onRequestClose}
                    className='react-modal-close'>
                    <img src={fechar} alt="Fechar   " />
                </button>
                <input
                    type='number'
                    placeholder='Valor'
                    value={amount}
                    onChange={event => setAmount(+event.target.value)}
                />
                <input
                    placeholder='Categoria'
                    value={category}
                    onChange={event => setCategory(event.target.value)}
                />
                <TransactionTypeContainer>
                    <RadioBox
                        type='button'
                        onClick={() => { setType('deposit') }}
                        isActive={type == 'deposit'}
                        activeColor="green"
                    >
                        <img src={entrada} alt="Entrada" />
                        <span>Entrada</span>
                    </RadioBox>
                    <RadioBox
                        type='button'
                        onClick={() => { setType('withdraw') }}
                        isActive={type == 'withdraw'}
                        activeColor="red"
                    >
                        <img src={saida} alt="Saida" />
                        <span>Saida</span>
                    </RadioBox>
                </TransactionTypeContainer>
                <button type="submit">Cadastrar</button>
            </Container>
        </Modal>
    )
}