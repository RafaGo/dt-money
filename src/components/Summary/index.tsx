import { Container } from "./styles";
import entrada from '../../assets/entradas.svg'
import saida from '../../assets/saidas.svg'
import total from '../../assets/total.svg'
import { updateVariableDeclarationList } from "typescript";
import { useContext } from "react";
import { TransactionsContext } from "../../TransactionsContext";



export function Summary() {
    const { transactions } = useContext(TransactionsContext)

    const summary = transactions.reduce((acc, transaction) => {
        if (transaction.type == 'deposit') {
            acc.deposits += transaction.amount;
            acc.total += transaction.amount;
        } else {
            acc.withdraws += transaction.amount;
            acc.total -= transaction.amount
        }
        return acc;
    }, {
        deposits: 0,
        withdraws: 0,
        total: 0,
    })

    return (
        <Container>
            <div>
                <header>
                    <p>Entradas</p>
                    <img src={entrada} alt="Entradas" />
                </header>
                <strong>  {new Intl.NumberFormat('pt-BR', {
                    style: 'currency',
                    currency: 'BRL'
                }).format(summary.deposits)}</strong>
            </div>
            <div>
                <header>
                    <p>Saidas</p>
                    <img src={saida} alt="Saidas" />
                </header>
                <strong> -   {new Intl.NumberFormat('pt-BR', {
                    style: 'currency',
                    currency: 'BRL'
                }).format(summary.withdraws)}</strong>
            </div>
            <div className="total-background">
                <header>
                    <p>Totais</p>
                    <img src={total} alt="Totais" />
                </header>
                <strong>  {new Intl.NumberFormat('pt-BR', {
                    style: 'currency',
                    currency: 'BRL'
                }).format(summary.total)}</strong>
            </div>
        </Container>
    )
}