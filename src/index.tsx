import React from 'react'
import ReactDOM from 'react-dom/client'
import { App } from './App'
import { Model, createServer } from 'miragejs'

createServer({
  models: {
    transaction: Model,
  },

  seeds(server){
    server.db.loadData({
    transactions:[{
      id:1,
      title:'Freelance',
      type:'deposit',
      category:'Dev',
      amount: 6000,
      createdAt:new Date('2023-11-22 09:37:00')
    },
    {
    id:2,
    title:'Aluguel',
    type:'wihdraw',
    category:'casa',
    amount: 1100,
    createdAt:new Date('2023-11-14')
    }
    ],
  })
  },

  routes() {
    this.namespace = 'api';

    this.get('/transactions', () => {
      return this.schema.all('transaction')
    })

    this.post('/transactions', (schema, request) => {
      const data = JSON.parse(request.requestBody)

      return schema.create('transaction',data);
    })
  }

})

ReactDOM.createRoot(document.getElementById('root')!).render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
)
