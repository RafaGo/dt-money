import { GlobalStyle } from "./styles/global"
import { Header } from "./components/Header"
import { Summary } from "./components/Summary"
import { TransactionTable } from "./components/TransactionsTable"
import { Dashboard } from "./components/Dashboard"
import { useState } from "react"
import { TransactionModal } from "./components/NewTransactionModal"
import { TransactionsProvider } from "./TransactionsContext"

export function App() {
  const [modalTransIsOpen, setModalTransIsOpen] = useState(false);

  function handleOpenModalTrans() {
    setModalTransIsOpen(true)
  }

  function handleCloseModalTrans() {
    setModalTransIsOpen(false)
  }

  return (
    <TransactionsProvider>
      <Header OpenNewTransactionModal={handleOpenModalTrans} />
      <Dashboard />
      <Summary /> 
      <TransactionTable />
      <TransactionModal
      isOpen={modalTransIsOpen} 
      onRequestClose={handleCloseModalTrans}/>
      <GlobalStyle />
    </TransactionsProvider>

  )
}

